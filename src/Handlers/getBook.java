package Handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import devjack.co.InventoryCommands.InvCmdsMain;

public class getBook {

	private InvCmdsMain pl;

	public getBook(InvCmdsMain instance) {
	pl = instance;

	}
	
	
	 private ItemStack item;
		
	private ItemStack createBook(int ItemID, String name){
		
		ItemStack stack = new ItemStack(ItemID);
		ItemMeta im = stack.getItemMeta();
		im.setDisplayName(name);
		stack.setItemMeta(im);	
		return stack;
	}
	

	public getBook(CommandSender s, Command c, String[] a) {
	
		if(s.hasPermission("invcmds.getbook")){
			if(a.length == 1){
							
					item = createBook(340, "§DInventory Commands");
					
					if(((Player)s).getInventory().contains(item)){
						return;
					}
					
					
					((Player)s).getInventory().addItem(item);
					s.sendMessage(pl.prefix + "Inventory Commands Book has been added.");
				
			}else{
				s.sendMessage(pl.prefix + "Incorrect Usage (/invcmds getbook)");
			}
			
		}else{
			s.sendMessage(pl.prefix + "No permission");
		}
	}
}
