package Handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import devjack.co.InventoryCommands.InvCmdsMain;

public class Remove {

	private InvCmdsMain pl;

 public Remove(InvCmdsMain instance){
	pl = instance;
 }
	
	
	public Remove(CommandSender s, Command c, String[] a) {
 
		
		if(s.hasPermission("invcmds.remove")){
			if(a.length == 2){
				
				String name = a[1];
				
				
				if(pl.DataConfig.contains(name)){
					
					pl.DataConfig.set(name, null);
					s.sendMessage(pl.prefix + "Inventory Command �5" + name + "�D removed." );
				}else{
					s.sendMessage(pl.prefix + "No command found!");
					s.sendMessage(pl.prefix + "Currently use must add the color codes to remove.");
				}
				
			}else{
				s.sendMessage(pl.prefix + "Incorrect Usage (/invcmds remove <name>)");
			}
			
		}else{
			s.sendMessage(pl.prefix + "No permission");
		}
		
		
		
		
		
	}

}
