package Handlers;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import devjack.co.InventoryCommands.InvCmdsMain;



public class AddCommand {

	private InvCmdsMain pl;

 public AddCommand(InvCmdsMain instance){
	pl = instance;
 }
	
	@SuppressWarnings("static-access")
	public AddCommand(CommandSender s, Command c, String[] a) {
		
		
		if(a.length > 1){
			if(s.hasPermission("invcmds.add")){

		String name = a[1];
		String id = a[2];
		
		StringBuilder sb = new StringBuilder();
		for(int z = 3; z < a.length; z++) {
		    sb.append(a[z] + " ");
		    
		}
		String command = sb.toString();
		
		
		
		if(!pl.DataConfig.contains("InventoryCommands." + name)){
			
			
			
			
			
			pl.DataConfig.set(name, "");
			pl.DataConfig.set(name + ".ItemID", id);
			pl.DataConfig.set(name + ".Command", command);
			
			s.sendMessage(pl.prefix + "Inventory Command added.");
			
			try {
				pl.DataConfig.save(pl.configFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
	}else{
		s.sendMessage(pl.prefix + "That name already exists!");	
		}
	}else{
		s.sendMessage(pl.prefix + "No permission");	
		}
	}else{
		s.sendMessage(pl.prefix + "Incorrect usage (/invcmds add <name> <id> <command>)");
	}
		
		
	}
		
		
	}

	
		
	
	
	
