package System;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import devjack.co.InventoryCommands.InvCmdsMain;




public class Events implements Listener {
	

	private InvCmdsMain pl;

	public Events(InvCmdsMain instance) {
	pl = instance;

	}
	 private Inventory inv;
	 private ItemStack item;
	
	private ItemStack createItem(int ItemID, String name){
		
		ItemStack stack = new ItemStack(ItemID);
		ItemMeta im = stack.getItemMeta();
		im.setDisplayName(name);
		stack.setItemMeta(im);	
		return stack;
	}
	
	
	
	@SuppressWarnings("static-access")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev){
		

		
		
		if(ev.getAction().equals(Action.RIGHT_CLICK_AIR) || ev.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			if(ev.getItem().getItemMeta().getDisplayName().equals("§DInventory Commands") ){
				if(!ev.getPlayer().hasPermission("invcmds.use")){
					return;
				}
				
				
				inv = Bukkit.getServer().createInventory(ev.getPlayer(), 18, "§5InventoryCommands");
				 
				 
				 for(String itemInConfig : pl.DataConfig.getKeys(false)){		
			
					 
					 
					 String ID = pl.DataConfig.getString(itemInConfig + ".ItemID");

					 int id2 = Integer.parseInt(ID);
					 
					 
					 
					 item = createItem(id2, itemInConfig.replace("&", "§"));
					 inv.addItem(item);					
				 }
				 ev.getPlayer().openInventory(inv);		
			}
		
		}
	}
	
	 @EventHandler
     public void onInventoryClick(InventoryClickEvent ev){
		
		  if (ev.getInventory().getName().equalsIgnoreCase(inv.getName())) {
			  
			  if(ev.getCurrentItem().getItemMeta() == null) return;
			  
			  String name = ev.getCurrentItem().getItemMeta().getDisplayName();
			  
			  if(pl.DataConfig.contains(name.replace("§", "&"))){
				  
				  String command = pl.DataConfig.getString(name.replace("§", "&") + ".Command");
				 
				 
				  
				  if(ev.getWhoClicked() instanceof Player){
					  Player clicker = (Player) ev.getWhoClicked();
					  
					  clicker.performCommand(command);
					}
				  
				  ev.getWhoClicked().closeInventory();
				  
				  ev.setCancelled(true);
				  
				  
			  }
			  
			  
		  }
	 }
	

	

}
