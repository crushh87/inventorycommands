package devjack.co.InventoryCommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import Handlers.AddCommand;
import Handlers.Help;
import Handlers.Remove;
import Handlers.getBook;

public class CmdExecutor implements CommandExecutor{
	
	public boolean onCommand(CommandSender s, Command c, String str, String[] a) {		
		if(a.length < 1){
			s.sendMessage("�5[�DInvCmds�5] �DInventory Commands by crushh87" );
			s.sendMessage("�5[�DInvCmds�5] �D/invcmds help - for help." );
			
		}	
		else if(a[0].equalsIgnoreCase("add")){
			new AddCommand(s,c,a);			
		}else if(a[0].equalsIgnoreCase("remove")){
			new Remove(s,c,a);
		}else if(a[0].equalsIgnoreCase("help")){
			new Help(s,c,a);
		}else if(a[0].equalsIgnoreCase("getBook")){
			new getBook(s,c,a);
		}
		
		return false;
	}
}
